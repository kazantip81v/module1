window.onload = function() {
	console.log('init js');

	/* Task4 */
	function pow (a, b) {
		var i,
			result = a;

		for (i = 0 ; i < b; i++) {
			result *= a;
		}

		return result;
	}

	console.log( pow(2, 5) );


	/* Task5 */
	function equality (a, b) {
		return a === b;
	}

	console.log( equality(2, 2) );
	console.log( equality(2, 5) );


	/* Task7 */
	(function () {
		var i,
			x = 1,
			length = 50;

		while (x <= length) {
			console.log(x);
			x++;
		}

		for (i = 1; i <= length; i++) {
			console.log(i);
		}
	})();


	/* Task8 */
	var obj = {
		'Оттава': 'Канада',
		'Киев': 'Украина'
	}

	function  capitalCountry (obj) {
		var country,
			capital;

		for (key in obj) {
			console.log(key + ' - это ' + obj[key]);
		}
	}

	capitalCountry(obj);


	/* Task10 */
	var array = [2, 5, 9, 15, 0, 4];

	function filterArray (arr) {
		var i,
			length = arr.length;

		for (i = 0; i < length; i++) {
			if (arr[i] > 3 && arr[i] < 10) {
				console.log(arr[i]);
			}
		}
	}

	filterArray(array);


	/* Task12 */
	var arrWords = ['я', 'учу', 'javascript', '!'];

	function compilationString (arr, separator) {
		var newString = arr.join(separator);

		return newString;
	}

	console.log( compilationString(arrWords, '+') );
	console.log( compilationString(arrWords, ' ') );


	/* Task13 */
	var arrLetter = ['a', 'b', 'c'],
		arrNumber = [1, 2, 3];

	function addArrayToArray (arr1, arr2) {
		var newArray  = [];

		return newArray = arr1.concat(arr2);
	}

	console.log( addArrayToArray(arrLetter, arrNumber) );


	/* Task14 */
	function pushArrayToArray (arr) {
		var newArray = arr.push(1, 2, 3);

		return arr;
	}

	console.log( pushArrayToArray(arrLetter) );


	/* Task15 */
	var arr = [1, 2, 3, 4, 5, 6 ];

	arr.forEach(function (i) {
		setTimeout(function () {
			console.log(i);
		}, 0);
	});


	/*Task16*/
	//1. Что будет в console = 5 - b is global variable. Исправить.
	(function() {
		//var a = b = 5;
		var b = 5,
			a = b;
	})();

	//console.log(b); // b is not defined


	/* Task17 */
	function findElement (array, elem) {
		var i,
			index,
			count = 0;

		for (i = 0; i < array.length; i++) {
			if (array[i] === elem) {
				count++;
				console.log('arr[' + i + ']');
			}
		}

		if (!count) {
			console.log('Not found')
		}
	}

	findElement([1, 2, 5, 9, 4, 13, 4, 10], 4);


	/* Task18 */
	var prizes = ['A Unicorn!', 'A Hug!', 'Fresh Laundry!'],
		btnIndex;

	for (btnIndex = 0; btnIndex < prizes.length; btnIndex++) {
		document.getElementById('btn-' + btnIndex).innerHTML = prizes[btnIndex];
		document.getElementById('btn-' + btnIndex).onclick = function(i) {
			return function () {
				console.log(prizes[i]);
			};
		}(btnIndex);
	}


	/* Task6 */
	function recursionOutputArray (arr, i) {
		if (i !== arr.length) {
			console.log(arr[i]);
			return recursionOutputArray(arr, i + 1);
		}
	}

	recursionOutputArray([6, 5, 4, 3, 2, 1], 0);


	/* Task9 */
	var helloWorld = ['Hello', 'world', '!'];

	function arrayToString (arr) {
		var i,
			length = arr.length,
			newString = '';

		for (i = 0; i < length; i++) {
			if (i === length - 1) {
				newString = newString.slice(0, -1);
				newString += arr[i];
			} else {
				newString += arr[i] + ' ';
			}
		}
		
		return newString;
	}

	console.log( arrayToString(helloWorld) );


	/* Task11 */
	function checkMatch (array, elem) {
		var i,
			text;

		for (i = 0; i < array.length; i++) {
			if (array[i] === elem) {
				return text = 'Present!';
			}
		}

		return text = 'Not found';
	}

	console.log( checkMatch([1, 2, 5, 9, 4, 13, 4, 10], 4) );


	/* Task19 */
	function findElements (arr1, arr2) {
		var i,
			j,
			lengthArr1 = arr1.length,
			lengthArr2 = arr2.length,
			newArr = [];

		for (i = 0; i < lengthArr1; i++) {
			for (j = 0; j < lengthArr2; j++) {
				if (arr1[i] === arr2[j]) {
					newArr.push(arr1[i]);
				}
			}
		}
		return newArr;
	}

	console.log( findElements([1, 2, 5, 9, 4, 13, 4, 10], [7, 8, 3, 9, 10, 13, 21]) );
};

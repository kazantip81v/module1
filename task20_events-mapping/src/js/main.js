window.onload = function() {
	var elements = {
		prev: document.getElementById('prev'),
		current: document.getElementById('current'),
		next: document.getElementById('next')
	};

	var events = {
		prev: function () {
			console.log('prev');
		},

		current: function () {
			console.log('current');
		},

		next: function () {
			console.log('next');
		}
	};
	// events mapping
	for (var key in elements) {
		(function () {
			var i = key;

			elements[i].onclick = function () {
				events[i]();
			}
		})();
	}

	/* Task21 */
	var btnAddVal = document.getElementById('add-value');
	var input_1 = document.getElementById('change-value');

	btnAddVal.onclick = function () {
		input_1.setAttribute('value', 'You clicked the button');
	}

	/* Task22 & Task23 */
	var btnSynchronVal = document.getElementById('synchron');
	var synchronVal_1 = document.getElementById('synchron-val-1');
	var synchronVal_2 = document.getElementById('synchron-val-2');
	var inputs = document.querySelectorAll('.change-color');

	btnSynchronVal.onclick = function () {
		var value = synchronVal_1.value;

		synchronVal_2.setAttribute('value', value);
		inputs.forEach(function (elem, i, arr) {
			arr[i].setAttribute('style', 'color: red');
		});
	}
};